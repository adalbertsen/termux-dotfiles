# adalbertsen's Termux dotfiles

I found this neat little app for Android [called Termux](https://wiki.termux.com/wiki/Main_Page) and I set up most of the stuff the same way I have on my main machine, but I got tired of typing on my phone, so I just made this repo.

## Differences between Termux and Linux

There is a nice article about this [on the wiki](https://wiki.termux.com/wiki/Differences_from_Linux).

## Todo

- Clean up `packages.txt`
- Check for more config files to sync between laptop and phone
